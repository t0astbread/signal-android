# Signal Android 

__THIS IS JUST MY OWN LITTLE FORK OF THE SIGNAL APP, NOT THE OFFICIAL DISTRIBUTION!__  
__The changes in this fork are purely aesthetical, that's why I'm not hosting this on GitHub or upstreaming my changes.__

__You can find the original repository over at [github.com/signalapp/Signal-Android](https://github.com/signalapp/Signal-Android).__

Signal is a messaging app for simple private communication with friends.

Signal uses your phone's data connection (WiFi/3G/4G) to communicate securely, optionally supports plain SMS/MMS to function as a unified messenger, and can also encrypt the stored messages on your phone.

## Screenshots

![Screenshot of conversation list activity](screenshots/1_scaled.png)
![Screenshot of preferences](screenshots/2_scaled.png) 
![Screenshot of notification preferences](screenshots/3_scaled.png) 

## License

Copyright 2011 Whisper Systems

Copyright 2013-2017 Open Whisper Systems

Licensed under the GPLv3: http://www.gnu.org/licenses/gpl-3.0.html

Google Play and the Google Play logo are trademarks of Google Inc.
